Ver una linea
git log --oneline

ver archivos modificados
git log --oneline --stat

Ver modificaciones de cada commit
git log --oneline --stat -p

Quitar archivos de staging
git reset NOMBREARCHIVO

Ver solo cambios de un log
git show #HASH

Ver último log
git show head

Ver penúltimo log
git show HEAD~1

BRANCH

Listar branches
git branch

Crear branch
git branch NOMBREBRANCH

Cambiar de branch
git checkout NOMBREBRANCH

Borrar branch
git branch -d NOMBREBRANCH

Crear branch y cambiar a esta
git checkout -b NOMBREBRANCH

Ver cambios no commiteados
git diff

Ver cambios en staging
git diff --cached

Merge
git merge BRANCHOTRABRANCH


CHERRY-PICK
Merge de commits específicos
git cherry-pick -en HASCHCOMMIT HASHCOMMIT2
Cancelar operación
git cherry-pick --abort


REVERTIR CAMBIOS
git revert HASHCOMMIT HASHCOMMIT


REBASE (reestablecer commit base)
Agrupa los commits posteriores
git rebase -i HEAD ~5
	pick: tomar commit
	drop: despreciar commit
	squash: juntar commit con commit anterior
	

Ignorar carpetas y archivos 
touch .gitignore
AGREGAR CARPETA Y ARCHIVOS QUE SE QUIEREN IGNORAR
